//#define WOKWI
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pico/float.h"     
#include "pico/double.h"    
#define pi 3.14159265359    //Use “3.14159265359” as the default basis for comparison.

float result_float();   // Initialize result_float functions
double result_double();  // Initialize result_double functions

int main() {

#ifndef WOKWI
    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
#endif

    // Print a console message to inform user what's going on.
    result_float();
    result_double();
    // Returning zero indicates everything went okay.
    return 0;
}

float result_float() {
        float result = 1;
        //Use “100000” as the default maximum number of iterations to perform
        //Calculate the approximation error for the single-precision representation 
        for(int i= 1; i <= 100000; i++){
            float part= (2.0*i/(2.0*i-1.0)) * (2.0*i/(2.0*i+1.0));
            result = result * part;
        }
        //Print the calculated value for PI (using single-precision) to the console
        printf("The result(single-precision) is: %f.\n",result*2.0);
        printf("the result(single-precision) approximation error is: %f.\n", (result*2.0 - pi));
    return result;
}

double result_double() {  
        double result = 1;
        //Use “100000” as the default maximum number of iterations to perform
        //Calculate the approximation error for the double-precision representation
        for(int i= 1; i <= 100000; i++){
            double part = (2.0*i/(2.0*i-1.0)) * (2.0*i/(2.0*i+1.0));
            result = result * part;
        }
        //Print the calculated value for PI (using double-precision) to the console.
        printf("The result(double-precision) is: %f.\n",result*2.0);
        printf("the result(double-precision) approximation error is: %f.\n", (result*2.0 - pi));
    return result;
}